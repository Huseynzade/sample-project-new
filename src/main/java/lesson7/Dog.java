package lesson7;

public class Dog extends Animal {
    @Override
    public String toString() {
        return "Hello I am " +
                "dog , my name is ='" + name + '\'' +
                ';';
    }

    public Dog(String name) {
        super(name);
    }
}
