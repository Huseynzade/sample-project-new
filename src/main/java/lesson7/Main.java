package lesson7;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Fish fish = new Fish("Baliq");
        Cat cat = new Cat("Pishik");
        Dog dog = new Dog("Buldoq");
        Animal a = new Animal("BIg Dog");
        Animal dragon = new Animal("Dragon"){
            @Override
            public String toString() {
                return "Hello I am a dragon, my name is ='" + name + '\'' + ';';
            }
        };
        ArrayList<Animal> animal = new ArrayList();
        animal.add(a);
        animal.add(cat);
        animal.add(fish);
        animal.add(dog);
        animal.add(dragon);
        for (Animal a2 : animal) {
            System.out.println(a2);
        }
    }
}
