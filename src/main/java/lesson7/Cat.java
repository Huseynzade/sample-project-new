package lesson7;

public class Cat extends Animal {

    @Override
    public String toString() {
        return "Hello I am " +
                "cat , my name is ='" + name + '\'' +
                ';';
    }

    public Cat(String name) {
        super(name);
    }
}
