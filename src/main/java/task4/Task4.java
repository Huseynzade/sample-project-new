package task4;


import java.nio.charset.Charset;
import java.util.Random;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Random r = new Random();
        int start = 97;
        int end = 122;
        StringBuffer sb = new StringBuffer(n);
        for (int i = 0; i < n; i++) {
            int lowerCase = start + (int) (r.nextFloat() * (end - start + 1));
            sb.append((char) lowerCase);
        }
//
//        System.out.println(sb.toString().toLowerCase());
//        System.out.println(sb.toString().toUpperCase());
//        System.out.println(sb.toString());
        String str1 = sb.toString().toLowerCase();
        String str2 = sb.toString().toUpperCase();
        StringBuilder sbuld = new StringBuilder(n);
        for (int i = 0; i < str1.length(); i++) {
            sbuld.append(str1.charAt(i));
            if (i < str2.length()) {
                sbuld.append(str2.charAt(i));
            }
        }
        System.out.println(sbuld.toString());
    }
}
