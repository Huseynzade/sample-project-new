package lesson9;

import java.util.ArrayList;
import java.util.Collections;

public class RandomNames {
    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<String>();
        names.add("Shaiq");
        names.add("vagif");
        names.add("Nazim");
        names.add("Malik");
        names.add("Aysu");
        System.out.println(names);
        ArrayList<String> randomNames = new ArrayList<String>(names);
        Collections.shuffle(randomNames);
        System.out.println(randomNames);
//        randomNames.addAll()
    }
}
