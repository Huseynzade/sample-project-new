package lesson09.warmup.t1;

public class Formatter2 extends Formatter0 {

    @Override
    public String formatEle(String string, Formatter0 formatter0) {
        return string.toUpperCase();
    }
}
