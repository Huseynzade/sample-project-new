package lesson11;

import java.util.ArrayList;
import java.util.List;

public class SentencesApp {
    public static void main(String[] args) {
        List<String> subjects = new ArrayList();
        subjects.add("Noel");
        subjects.add( "The cat");
        subjects.add( "The dog");
        List<String> verbs = new ArrayList();
        verbs.add("wrote" );
        verbs.add("chased");
        verbs.add( "slept on" );
        List<String> objects = new ArrayList();
        objects.add("the book");
        objects.add( "the ball");
        objects.add( "the bed");
        List<String> sentences = new ArrayList();
        sentences.addAll(subjects);
        sentences.addAll(verbs);
        sentences.addAll(objects);
        System.out.println(sentences);
        for(int i=0;i<sentences.size();i++){
            for(int j=3;j<6;j++){
                for(int k=6;k<9;k++){
                    System.out.println(sentences.get(i)+ " " + sentences.get(j) + " " + sentences.get(k) + " ;");

                }
            }
        }
    }

}
