package lesson8;

import java.util.Random;

public class Point {
    Random random = new Random();
    int x=random.nextInt()  ;
    int y=random.nextInt() ;
    int z=random.nextInt()  ;
    public Point(int x){
        this.x=x;

    }
    public Point(){
        this.x=(int)(Math.random()*10)+1;
        this.y=(int)(Math.random()*10)+1;
        this.z=(int)(Math.random()*10)+1;
    }


    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
