package lesson8;

public class Triangle extends Figure {
    Point p1;
    Point p2;
    Point p3;
    int count;
    public Triangle(Point p1,Point p2,Point p3){
        this.p1=p1;
        this.p2=p2;
        this.p3=p3;
        count++;
    }

    @Override
    public double area() {
        double a = Math.abs(p1.x-p2.x);
        double b = Math.abs(p1.y-p2.y);
        double c = p2.z-p3.z;
        double p = (a+b+c)/2;
        return p ;
    }
}
