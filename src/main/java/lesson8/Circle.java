package lesson8;

public class Circle extends Figure {
    Point point;
    int radius;
    static  int count;
    public Circle(Point point){
        this.radius=(int)(Math.random()*10)+1;
        this.point=point;
        count ++;
    }

    @Override
    public double area() {
        return Math.PI*Math.pow(radius,2);
    }
}
