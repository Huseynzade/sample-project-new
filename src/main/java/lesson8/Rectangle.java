package lesson8;

public class Rectangle extends Figure {
    Point p1;
    Point p2;
    int count;
    public Rectangle(Point p1, Point p2){
        this.p1=p1;
        this.p2=p2;
        count++;
    }

    @Override
    public double area() {
        return (p1.x-p2.x)*(p1.y-p2.y);
    }
}
