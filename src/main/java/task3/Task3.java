package task3;

public class Task3 {
    public static void main(String[] args) {
        int height = 8;
        int width = 22;
        double tang = ((double) width) / height;
        StringBuilder str = new StringBuilder();
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width  ; j++) {
                if (i == 1 || i == height || j == 1 || j == width || j == (int) (i * tang) || (int) (i * tang) == width - j + 1) {
                    str.append("*");
                } else {
                    str.append(" ");
                }
            }
            str.append("\n");
        }
        System.out.println(str);
    }
}
