package task1;

import java.util.Scanner;

public class Task {
    public static void main1(String[] args) {
        System.out.println("Hello, what is your name?");
        Scanner in = new Scanner(System.in);
        String name=in.nextLine();
        System.out.println("Hello " + name +"\nNice to meet you " +name + "\nBye");

    }

    public static void main(String[] args) {
        System.out.println("How old are you? ");
        Scanner in = new Scanner(System.in);
        int age = in.nextInt();
        if(age<18){
            System.out.println("Go to the cinema");
        }else{
            System.out.println("Go to the night club");
        }
    }
}
