package task5;

import java.util.Arrays;

public class ArraysPlusMinus {
    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 200) - 100;
        }
        System.out.println(Arrays.toString(array));
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < 10; j++) {
                if (array[i] < 0) {
                    if (array[j] < 0) {
                        int t;
                        t = array[i];
                        array[i] = array[j];
                        array[j] = t;
                        break;
                    }
                }
            }
        }
        System.out.println(Arrays.toString(array));

        for (int i = 9; i < array.length - 1; i--) {
            if (array[i] > 0) {
                for (int j = i -1; j < 10; j--) {

                    if (array[j] > 0) {
                        int t;
                        t = array[i];
                        array[i] = array[j];
                        array[j] = t;

                    }
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(array));

    }


}

