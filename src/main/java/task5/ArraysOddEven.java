package task5;

import java.util.Arrays;
import java.util.Scanner;

public class  ArraysOddEven {
    public static void main(String [] args) {
        Scanner in = new Scanner(System.in);
        int SIZE = in.nextInt();
//        Random random = new Random();
        int odd[] = new int[SIZE];
        int even[] = new int[SIZE];
        int combine[]=new int[SIZE*2];
        int i;
        for (i= 0; i <2* SIZE ; i++) {
            int r = (int)(Math.random()*100)+1;
            if (r % 2 == 0) {
                    even[i/2] = r;
            } else {
                    odd[i/2] = r;
            }
        }
        System.out.println();
        for (int j = 0; j < SIZE; j++ ) {
            combine[j*2] = even[j];
            combine[j*2+1] = odd[j];
        }
        System.out.println(Arrays.toString(even));
        System.out.println(Arrays.toString(odd));
        System.out.println(Arrays.toString(combine));
    }
}


